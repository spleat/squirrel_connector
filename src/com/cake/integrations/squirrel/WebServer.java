package com.cake.integrations.squirrel;

import java.net.InetSocketAddress;

import com.sun.net.httpserver.HttpServer;

/**
 * This is the class which executes on Squirrel POS box. It runs a web server on port 8080, receives inbound HTTP requests
 * and translates those into local POS requests. It then opens a port to listen for the response and writes that response
 * back on the HTTP response.
 * 
 * To build the JAR after changes, run:
 * 		jar cf squirrel-cake.jar -C bin com
 * 
 * @author jonno
 * @date 2015-04-21
 *
 */
class WebServer {
   public static void main(String argv[]) throws Exception {

      new WebServer().startServer();
   }

   private static final int WEB_SERVER_PORT = 8080;

   public void startServer() {
      try {
         HttpServer server = HttpServer.create(new InetSocketAddress(WEB_SERVER_PORT), 50);
         server.createContext("/tabs", new CreateTabHandler());
         server.createContext("/tab/", new QueryTabHandler());
         server.createContext("/payments", new CreatePaymentHandler());
         server.createContext("/check/", new QueryCheckHandler());
         server.createContext("/status", new ConnectionTestHandler());
         server.start(); // Starts server in background thread
         System.out.println("Web server started on port " + WEB_SERVER_PORT);
      } catch (Exception e) {
         e.printStackTrace();
      }
   }
}