package com.cake.integrations.squirrel;

public class ServerException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public int responseCode;
	public String error;
	
	public ServerException(int responseCode, String error) {
		this.responseCode = responseCode;
		this.error = error;
	}
}
