package com.cake.integrations.squirrel;

import java.net.URI;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.w3c.dom.Element;

public class ConnectionTestHandler extends CakeHttpHandler {

	@Override
	protected String httpMethod() {
		return "GET";
	}

	@Override
	protected JSONObject generateServerResponse(URI uri) throws ServerException {
		JSONObject json = lookupDepartments();
		checkGatewayStatus(json, uri);
		return json;
	}
	
	@SuppressWarnings("unchecked")
	private void checkGatewayStatus(JSONObject json, URI uri) throws ServerException {
		String rxml = new XmlRequestGenerator(uri).supplyVersionXml();
		String xml = new RawPortConnector().sendXml(rxml); // throws ServerException		
		Element element = parseXml(xml).getDocumentElement();
		json.put("version", element.getAttribute("version"));
	}

	@SuppressWarnings("unchecked")
	private JSONObject lookupDepartments() throws ServerException {
		String sql = "SELECT DeptNo, Name FROM K_Department WHERE DeptNo <> 0 AND Status=0 ORDER BY Name";
		Statement stmt = null;
		ResultSet rs = null;
		Connection connection = null;
		int number = 0;
		JSONObject json = new JSONObject();
		try {
			// Establish the connection.
			connection = buildConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			JSONArray array = new JSONArray();
			while (rs.next()) {
				number++;
				JSONObject row = new JSONObject();
				row.put("number", number);
				row.put("id", rs.getInt(1));
				row.put("name", rs.getString(2));
				array.add(row);
			}
			json.put("departments", array);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new ServerException(500, "Database connectivity failed: " + e. getMessage());
		} finally {
			try {
				rs.close();
				stmt.close();
				connection.close();
			} catch(Exception e) {}
		}
		return json;
	}
}
