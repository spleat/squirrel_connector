package com.cake.integrations.squirrel;

import java.net.URI;
import java.util.HashMap;

/**
 * 
 * 
 * <?xml version="1.0" ?>
<AddToOrder version="1.40" device="1" nolog="false">
<Order badge="#{badge}" department="#{department}" name="#{order_name}" table="-1" covers="1">
<Department num="#{department}">
<Seat num="1">
<Item id="#{cake_item_id}" price="0.00" name="#{item_name}"/>
</Seat>
</Department></Order></AddToOrder>

Sending <?xml version="1.0" ?>
<AddToOrder version="1.40" device="1" nolog="false">
<Order badge="949" department="1" name="Jonathon" table="-1" covers="1">
<Department num="1">
<Seat num="1">
<Item id="181" price="0.00" name="Cake Tab Jonathon"/>
</Seat>
</Department></Order></AddToOrder>

Received <?xml version="1.0" ?><XMLError version="1.40" success="false" error="java.io.EOFException: no more input"/>

 * @author User
 *
 */
public class XmlRequestGenerator {

	private static final String API_VERSION = "1.30";
	
	public HashMap<String, String> params;
	
	public XmlRequestGenerator(URI uri) {
		this.params = decodeQueryParamsFromUri(uri);
	}
	
	public String supplyVersionXml() {
		return "<?xml version=\"1.0\" ?> <SupplyVersion device=\"1\"/>";
	}
	
	protected String createTabXml() throws ServerException {
		validateParametersExist("badge", "department", "order_name", "cake_item_id");
		String badge = params.get("badge");
		String department = params.get("department");
		String order_name = params.get("order_name");
		String cake_item_id = params.get("cake_item_id");
		
		return "<?xml version=\"1.0\" ?><AddToOrder version=\"" + API_VERSION + "\" device=\"1\" nolog=\"false\">" +
				"<Order badge=\"" + badge + "\" department=\"" + department + "\" name=\"" + order_name + "\" table=\"-1\" covers=\"1\">" +
				"<Department num=\"" + department + "\">" +
				"<Seat num=\"1\">" +
				"<Item id=\"" + cake_item_id + "\" price=\"0.00\" name=\"Cake Tab " + order_name + "\"/>" +
				"</Seat></Department></Order></AddToOrder>";
	}
	
	protected String oneTableXml() throws ServerException {
		String order_name = params.get("order_name");
		return "<?xml version=\"1.0\" ?><OneTable version=\"" + API_VERSION + "\" device=\"1\" name=\"" + order_name + "\" />";
	}
	
	public String queryTabXml() throws ServerException {
		validateParametersExist("department", "id", "badge");
		String badge = params.get("badge");
		String department = params.get("department");
		String id = params.get("id");
		return "<?xml version=\"1.0\" ?><Recall version=\"" + API_VERSION + "\" device=\"1\" department=\"" + department + "\" " +
				"table=\"" + id + "\" badge=\"" + badge + "\" nolog=\"false\" />";
	}
	
	public String createPaymentXml(String tableNo, int departmentNo, int covers) throws ServerException {
		validateParametersExist("badge", "amount", "tip", "payment_id");
		String badge = params.get("badge");
		String amount = params.get("amount");
		String tip = params.get("tip");
		String payment_id = params.get("payment_id");
		return "<?xml version=\"1.0\" ?><AddToOrder version=\"" + API_VERSION + "\" device=\"1\">"
		+ "<Order badge=\"" + badge + "\" department=\"" + departmentNo + "\" table=\"" + tableNo + "\" covers=\"" + covers + "\">"
		+ "<Payment id=\"" + payment_id + "\" amount=\"" + amount + "\" tip=\"" + tip + "\" /></Order></AddToOrder>";		
	}
	
	public String openTablesXml(int departmentNo) {
		String badge = params.get("badge");
		return "<?xml version=\"1.0\" ?><OpenTables version=\"" + API_VERSION + "\" device=\"1\" badge=\"" + badge + "\" department=\"" + departmentNo + "\" showtables=\"true\"/>";
	}
	
	public int idFromParams() throws ServerException {
		validateParametersExist("id");
		return Integer.parseInt(params.get("id"));
	}

	protected HashMap<String, String> decodeQueryParamsFromUri(URI uri) {
		HashMap<String, String> params = new HashMap<String, String>();
		String query = uri.getRawQuery();
		if (query != null) {
			for (String pair : query.split("&")) {
				String[] split = pair.split("="); 
				if (split.length > 1) {
					params.put(split[0], split[1]);
				} else {
					params.put(split[0], null);
				}
			}
		}
		
		return params;
	}

	protected void validateParametersExist(String... names) throws ServerException {
		for (String name : names) {
			if (params.get(name) == null) {
				throw new ServerException(400, "Missing expected parameter '" + name + "'");
			}
		}
	}
}