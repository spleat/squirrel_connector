package com.cake.integrations.squirrel;

import java.net.URI;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.json.simple.JSONObject;
import org.w3c.dom.Element;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import com.microsoft.sqlserver.jdbc.SQLServerException;

/**
 * Create a tab in the POS.
 * 
 * The request XML will be:
 * <?xml version="1.0" ?><OneTable version="1.30" device="1" name="Karim Idr" />
 * 
 * and the response will be:
 * <?xml version="1.0" ?><OneTableResult version="1.40" device="1" success="true"><Table department="1" dname="BAR" table="3" name="Karim Idr" status="busy"/></OneTableResult>
 * 
 * @author Jonno
 *
 */
public class CreateTabHandler extends CakeHttpHandler {

	@Override
	protected JSONObject generateServerResponse(URI uri) throws ServerException {
		
		XmlRequestGenerator generator = new XmlRequestGenerator(uri);
		
		// send the create tab XML
		String xml = new RawPortConnector().sendXml(generator.createTabXml());
		checkForError(xml); // check there is no error, but don't care about the response otherwise
		
		try {
			Thread.sleep(500); // wait a fraction of a second for the POS to sort its shit
		} catch(InterruptedException e) { }

		return queryLatestCheck();
	}
	
	private void checkForError(String xml) throws ServerException {
		checkForError(parseXml(xml).getDocumentElement());
	}
	
	private void checkForError(Element element) throws ServerException {
		if (element.getAttribute("success").equals("false")) {
			throw new ServerException(400, element.getAttribute("error"));
		}
	}
	
	@SuppressWarnings("unchecked")
	private JSONObject queryLatestCheck() throws ServerException {
		String sql = "SELECT TOP 1 h.CheckID, h.Active, c.TableID FROM X_CheckTable t INNER JOIN X_CheckHeader h ON t.CheckID=h.CheckID " +
				"LEFT OUTER JOIN X_CurrentCheck c ON c.CheckID=h.CheckID WHERE h.IsCurrent=1 AND h.Active=1 ORDER BY h.CheckID DESC";

		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			connection = buildConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);

			JSONObject json = new JSONObject();
			if (rs.next()) {
				json.put("id", rs.getInt(1));
				json.put("active", rs.getBoolean(2)); // true If This Check Is Still Open
				json.put("table", rs.getInt(3));
			} else {
				throw new ServerException(404, "Active check not found");
			}
			return json;				
		} catch (SQLException e) {
			e.printStackTrace();
			throw new ServerException(500, "Database connectivity failed: " + e. getMessage());
		} finally {
			try {
				rs.close();
				stmt.close();
				connection.close();
			} catch(Exception e) {}
		}
	}

	@Override
	protected String httpMethod() {
		return "POST";
	}
}