package com.cake.integrations.squirrel;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class TestConnection {

	public void testStatus() throws Exception {
		URL url = new URL("http://localhost:8080/status");
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestProperty("Authorization", "Token token=\"rM4ma2ndp3cn3adk22sldm\"");
		conn.setRequestMethod("GET");
		BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		String result = "";
		String line = null;
		while ((line = rd.readLine()) != null) {
            result += line;
         }
         rd.close();
         System.out.println(result);
    }

	public static void main(String args[]) throws Exception {
		new TestConnection().testStatus();
	}
}
