package com.cake.integrations.squirrel;

import java.net.URI;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.microsoft.sqlserver.jdbc.SQLServerException;

/**
 * 
 * @author Jonno
 *
 */
public class QueryTabHandler extends CakeHttpHandler {
	
	private Connection connection;
	private int checkNo;
	private int tableId;

	@Override
	protected JSONObject generateServerResponse(URI uri) throws ServerException {
		// extract the check number from the URI 
		checkNo = idFromUri(uri);
		
		// determine if this check has been transferred to another check
		followCheckTransfer();
		
		// query the database for tab using the ID
		JSONObject json = queryDatabaseForCheck();

		// lookup the items on the tab
		addItemsToObject(json);
		
		// lookup the payments on the tab
		addPaymentsToObject(json);
		
		closeDatabaseConnection();
		
		// return the tab json
		return json;
	}
	
	private void followCheckTransfer() throws ServerException {
		String sql = "SELECT CheckTo FROM X_TransferLog WHERE CheckFrom=" + checkNo + " ORDER BY ID DESC";

		Statement stmt = null;
		ResultSet rs = null;

		try {
			// Establish the connection. 
			stmt = connection().createStatement();
			rs = stmt.executeQuery(sql);

			if (rs.next() && rs.getInt(1) != checkNo) {
				checkNo = rs.getInt(1);
				followCheckTransfer(); // in case it's been transferred more than once
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new ServerException(500, "Database connectivity failed: " + e. getMessage());
		} finally {
			if (rs != null) try { rs.close(); } catch(Exception e) {}
			if (stmt != null) try { stmt.close(); } catch(Exception e) {}
		}
	}
	
	@SuppressWarnings("unchecked")
	private JSONObject queryDatabaseForCheck() throws ServerException {
		String sql = "SELECT h.CheckID, h.Active, c.TableID FROM X_CheckHeader h LEFT OUTER JOIN X_CurrentCheck c ON c.CheckID=h.CheckID WHERE h.IsCurrent=1 AND h.CheckID=" + checkNo;

		Statement stmt = null;
		ResultSet rs = null;

		try {
			// Establish the connection. 
			stmt = connection().createStatement();
			rs = stmt.executeQuery(sql);

			JSONObject json = new JSONObject();
			if (rs.next()) {
				json.put("id", rs.getInt(1));
				json.put("active", rs.getBoolean("Active")); // true If This Check Is Still Open
				tableId = rs.getInt(3);
				if (tableId > 0) {
					json.put("table", rs.getInt(3));
				} // else it's been removed from the table - i.e. no longer current
			} else {
				throw new ServerException(404, "Check with ID " + checkNo + " not found");
			}
			return json;				
		} catch (SQLException e) {
			e.printStackTrace();
			throw new ServerException(500, "Database connectivity failed: " + e. getMessage());
		} finally {
			if (rs != null) try { rs.close(); } catch(Exception e) {}
			if (stmt != null) try { stmt.close(); } catch(Exception e) {}
		}
	}

	@SuppressWarnings("unchecked")
	private void addItemsToObject(JSONObject json) throws ServerException {
		String sql = "SELECT i.ItemID, i.GrossPrice, i.OriginalPrice, i.Quantity, c.Name, m.MenuType, m.OpenName, m.Name, o.Name, d.DiscountAmt, p.PromoAmt, s.ScAmt "
				+ "FROM X_CheckItem i INNER JOIN K_Menu m ON i.MenuID=m.MenuID "
				+ "INNER JOIN K_Category c ON m.CatID=c.CatID "
				+ "LEFT OUTER JOIN X_CheckOpenItem o ON i.ItemID=o.ItemID "
				+ "LEFT OUTER JOIN X_CheckDiscount d ON i.ItemID=d.ItemID "
				+ "LEFT OUTER JOIN X_CheckPromo p ON i.ItemID=p.ItemID "
				+ "LEFT OUTER JOIN X_CheckSrvcChrg s ON i.ItemID=s.ItemID "
				+ "WHERE m.Status=0 "; // non-zero if marked for later deletion
		if (tableId > 0) {
			sql += "AND i.CheckID IN (SELECT CheckID FROM X_CurrentCheck WHERE TableID=" + tableId + ")";
		} else {
			sql += "AND i.CheckID=" + checkNo;
		}
		Statement stmt = null;
		ResultSet rs = null;

		try {
			// Establish the connection. 
			stmt = connection().createStatement();
			rs = stmt.executeQuery(sql);
			JSONArray items = new JSONArray();

			while (rs.next()) {
				items.add(itemFromResultSet(rs));
			}
			json.put("items", items);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new ServerException(500, "Database connectivity failed: " + e. getMessage());
		} finally {
			if (rs != null) try { rs.close(); } catch(Exception e) {}
			if (stmt != null) try { stmt.close(); } catch(Exception e) {}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void addPaymentsToObject(JSONObject json) throws ServerException {
		String sql = "SELECT p.CheckPayID, p.PayAmt, p.TipAmt, t.Name, h.Name "
				+ "FROM K_Payment t INNER JOIN X_CheckPay p ON t.PaymentID=p.PaymentID "
				+ "INNER JOIN H_PayType h ON t.PaymentType=h.PaymentType "
				+ "WHERE ";
		if (tableId > 0) {
			sql += "p.CheckID IN (SELECT CheckID FROM X_CurrentCheck WHERE TableID=" + tableId + ")";
		} else {
			sql += "p.CheckID=" + checkNo;
		}
		Statement stmt = null;
		ResultSet rs = null;

		try {
			stmt = connection().createStatement();
			rs = stmt.executeQuery(sql);
			JSONArray payments = new JSONArray();

			while (rs.next()) {
				payments.add(paymentFromResultSet(rs));
			}
			json.put("payments", payments);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new ServerException(500, "Database connectivity failed: " + e. getMessage());
		} finally {
			if (rs != null) try { rs.close(); } catch(Exception e) {}
			if (stmt != null) try { stmt.close(); } catch(Exception e) {}
		}
	}

	@SuppressWarnings("unchecked")
	private JSONObject paymentFromResultSet(ResultSet rs) throws SQLException {
		JSONObject payment = new JSONObject();
		payment.put("id", rs.getInt(1));
		payment.put("amount", rs.getDouble(2)); // Amount tendered (ie $7.00 Bill = -$3.00 TipAmt + $10.00 PayAmt)
		payment.put("tip", rs.getDouble(3)); // Tip amount
		payment.put("type", rs.getString(4).trim());
		payment.put("category", rs.getString(5).trim());
		return payment;
	}

	@SuppressWarnings("unchecked")
	private JSONObject itemFromResultSet(ResultSet rs) throws SQLException {
		JSONObject item = new JSONObject();
		item.put("id", rs.getInt(1));
		item.put("price", rs.getDouble(3));
		item.put("quantity", rs.getDouble(4)); // How much was ordered (L)
		item.put("category", rs.getString(5).trim()); // Category Name
		item.put("menu_type", rs.getInt(6)); // 1=item,2=addon,3=modifier,4=volmod,5=transformer,6=nonitemtype (See H_MenuType)
		boolean openName = rs.getBoolean(7);
		item.put("name", rs.getString(openName ? 9 : 8).trim());
		item.put("discount", -1 * rs.getDouble(10) + rs.getDouble(11));
		item.put("service", rs.getDouble(12));
		return item;
	}
	
	private void closeDatabaseConnection() {
		try { 
			if (connection != null && !connection.isClosed()) {
				connection().close();
			}
		} catch(Exception e) {}
		connection = null;
	}

	private Connection connection() throws SQLServerException {
		if (connection == null) {
			connection = buildConnection();
		}
		return connection;
	}
		
	@Override
	protected String httpMethod() {
		return "GET";
	}	
}