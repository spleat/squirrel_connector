package com.cake.integrations.squirrel;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class RawPortConnector {

	private volatile boolean threadDone;
	private String response;
	private Thread thread;
	
	public String sendXml(String xml) throws ServerException {
		startResponseListener(); // start listening for the response
		System.out.println("Sending " + xml);
		sendRequestOnSocket(xml); // send the XML
		try {
			if (!threadDone) {
				thread.join(); // block waiting for server response
				System.out.println("Received response: " + response);
				return response;
			}
		} catch (InterruptedException e) {
			e.printStackTrace(); // believe this can only happen if the app is killed mid-request
		}
		System.out.println("Received response: " + response);
		return response;
	}

	/**
	 * Write the given XML to the local TCP 4998 socket
	 * @param xml
	 * @throws ServerException if writing to the socket fails
	 */
	private void sendRequestOnSocket(String xml) throws ServerException {

		Socket socket = null;
		try {
			socket = new Socket("localhost", 4998);
			PrintWriter writer = new PrintWriter(socket.getOutputStream());
			writer.write(xml);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new ServerException(500, e.getMessage());
		} finally {
			if (socket != null) {
				try { socket.close(); } catch (IOException e) { } // silently ignore
			}
		}
	}

	/**
	 * Spawn a thread waiting for a response from the POS on port 4999
	 */
	private void startResponseListener() {

		threadDone = false;
		thread = new Thread() {
			@Override
			public void run() {
				ServerSocket serverSocket = null;
				Socket socket = null;
				try {
					serverSocket = new ServerSocket(4999);
					socket = serverSocket.accept(); // blocks waiting for response
					StringWriter writer = new StringWriter();
					InputStreamReader reader = new InputStreamReader(socket.getInputStream());
					int c;
					while ((c = reader.read()) != -1) {
						writer.write(c);
					}
					response = writer.toString();
					System.out.println("Received response: " + response);
				} catch (IOException e) {
					System.out.println("Exception raised on response listener: " + e);
					e.printStackTrace();
				} finally {
					try {
						if (socket != null) {
							socket.close();
						}
						if (serverSocket != null) {
							serverSocket.close();
						}
					} catch (IOException e) { } // don't care
				}
				threadDone = true;
			}
		};
		thread.start();
	}
}
