package com.cake.integrations.squirrel;

import java.net.URI;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.json.simple.JSONObject;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * 
 * Response:
 * <?xml version="1.0" ?>
 * <ModifyOrderResult version="1.30" device="23" success="false" error="reason for failure" >
 * <Line text="Receipt text"/>
 * </ModifyOrderResult>
 * 
 * @author User
 *
 */
public class CreatePaymentHandler extends CakeHttpHandler {
	
	String tableName;
	int deptId;
	int covers;
	XmlRequestGenerator generator = null;
	
	@Override
	protected JSONObject generateServerResponse(URI uri) throws ServerException {
		generator = new XmlRequestGenerator(uri);
		
		int checkNo = generator.idFromParams();
		queryDatabaseForTable(checkNo);
		int departmentNo = departmentNumberFromId();
		
		String tableNo = tableNumberFromName(departmentNo);
		String rxml = generator.createPaymentXml(tableNo, departmentNo, covers);
		
		// send the Modify Tab XML
		String xml = new RawPortConnector().sendXml(rxml); // throws ServerException
		
		System.out.println("Received XML " + xml);
		
		// parse XML and check there was no error
		checkResponseFromXml(xml); // throws ServerException
		
		return new JSONObject();
	}
	
	private String tableNumberFromName(int departmentNo) throws ServerException {
		String rxml = generator.openTablesXml(departmentNo);
		String xml = new RawPortConnector().sendXml(rxml);
		NodeList nodeList = parseXml(xml).getElementsByTagName("Table");
		for (int i = 0; i < nodeList.getLength(); i++) {
			Element table = (Element)nodeList.item(i);
			if (table.getAttribute("name").equals(tableName)) {
				return table.getAttribute("table");
			}
		}
		throw new ServerException(500, "Table named " + tableName + " not found");
	}
	
	private int departmentNumberFromId() throws ServerException {
		String sql = "SELECT DeptNo FROM K_Department WHERE DeptNo <> 0 AND Status=0 ORDER BY Name";
		Statement stmt = null;
		ResultSet rs = null;
		Connection connection = null;
		int number = 0;
		boolean found = false;
		try {
			// Establish the connection.
			connection = buildConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				number++;
				if (rs.getInt(1) == deptId) {
					found = true;
					break;
				}
			}
			if (!found) {
				throw new ServerException(404, "Department with ID " + deptId + " not found");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new ServerException(500, "Database connectivity failed: " + e. getMessage());
		} finally {
			try {
				rs.close();
				stmt.close();
				connection.close();
			} catch(Exception e) {}
		}
		return number;
	}
	
	private void checkResponseFromXml(String xml) throws ServerException {
		Element element = parseXml(xml).getDocumentElement();
		if (element.getAttribute("success").equals("false")) {
			throw new ServerException(400, element.getAttribute("error"));
		}
	}

	private void queryDatabaseForTable(int checkNo) throws ServerException {
		String sql = "SELECT t.Name,c.DeptNo,h.Covers FROM X_CheckHeader h LEFT OUTER JOIN X_CurrentCheck c ON c.CheckID=h.CheckID LEFT OUTER JOIN X_CurrentTable t ON t.TableID=c.TableID WHERE h.IsCurrent=1 AND h.CheckID=" + checkNo;

		Statement stmt = null;
		ResultSet rs = null;
		Connection connection = null;
		
		try {
			// Establish the connection.
			connection = buildConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				tableName = rs.getString(1).trim();
				deptId = rs.getInt(2);
				covers = rs.getInt(3);
			} else {
				throw new ServerException(404, "Check with ID " + checkNo + " not found");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new ServerException(500, "Database connectivity failed: " + e. getMessage());
		} finally {
			try {
				rs.close();
				stmt.close();
				connection.close();
			} catch(Exception e) {}
		}
	}

	@Override
	protected String httpMethod() {
		return "POST";
	}
}
