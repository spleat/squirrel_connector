package com.cake.integrations.squirrel;

import java.net.URI;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.json.simple.JSONObject;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import com.microsoft.sqlserver.jdbc.SQLServerException;

public class QueryCheckHandler extends CakeHttpHandler {

	@Override
	protected String httpMethod() {
		return "GET";
	}

	@Override
	@SuppressWarnings("unchecked")
	protected JSONObject generateServerResponse(URI uri) throws ServerException {
		JSONObject json = new JSONObject();
		int id = idFromUri(uri);
		json.put("check", id);
		json.put("table", tableNumberFromCheckId(id));
		
		return json;
	}
	
	private int tableNumberFromCheckId(int checkId) throws ServerException {
		String sql = "SELECT TableID FROM X_CurrentCheck WHERE CheckID=" + checkId;

		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			connection = buildConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				return rs.getInt(1);
			}
			throw new ServerException(404, "Check not found with ID " + checkId);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new ServerException(500, "Database connectivity failed: " + e. getMessage());
		} finally {
			try {
				rs.close();
				stmt.close();
				connection.close();
			} catch(Exception e) {}
		}
	}
}
