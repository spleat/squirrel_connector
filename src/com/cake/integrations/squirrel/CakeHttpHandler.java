package com.cake.integrations.squirrel;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.sql.Connection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.json.simple.JSONObject;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import com.microsoft.sqlserver.jdbc.SQLServerException;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public abstract class CakeHttpHandler implements HttpHandler {

	private static final String CAKE_AUTH_TOKEN = "rM4ma2ndp3cn3adk22sldm";

	@Override
	public void handle(HttpExchange exchange) throws IOException {
		if (!exchange.getRequestMethod().equals(httpMethod())) {
			writeHttpResponse(exchange, 400, "Invalid " + exchange.getRequestMethod() + " request");			
		} else if (authorizeRequest(exchange)) {
			handleValidRequest(exchange);
		} else {
			// ignore bogus request
			writeHttpResponse(exchange, 401, "Unauthorized");
		}
	}

	protected abstract String httpMethod();
	protected abstract JSONObject generateServerResponse(URI uri) throws ServerException;

	private void handleValidRequest(HttpExchange exchange) throws IOException {

		try {
			JSONObject json = generateServerResponse(exchange.getRequestURI());
			writeHttpResponse(exchange, 200, json.toString());
		} catch (ServerException e) {
			e.printStackTrace();
			writeHttpResponse(exchange, e.responseCode, e.error);			   
		} catch (Exception e) {
			e.printStackTrace();
			writeHttpResponse(exchange, 500, e.getMessage());
		}
	}

	private void writeHttpResponse(HttpExchange exchange, int status, String body) throws IOException {
		exchange.sendResponseHeaders(status, body.length()); // HTTP OK response
		BufferedOutputStream out = new BufferedOutputStream(exchange.getResponseBody());
		for (int i = 0; i < body.length(); i++) {
			out.write(body.charAt(i));
		}
		out.close();
		exchange.close();
	}

	private boolean authorizeRequest(HttpExchange exchange) {
		Headers headers = exchange.getRequestHeaders();
		String auth = headers.getFirst("Authorization");
		return auth != null && auth.equals("Token token=\"" + CAKE_AUTH_TOKEN + "\"");
	}
	
	/**
	 * Extract the integer ID of the tab from the URI request.
	 * The URL path is expected to be /tab/123 
	 * @param uri
	 * @return
	 * @throws ServerException
	 */
	protected int idFromUri(URI uri) throws ServerException {
		String[] parts = uri.getPath().split("/");
		String sId = parts[parts.length - 1];
		int id = Integer.parseInt(sId);
		if (id > 0) {
			return id;
		}
		throw new ServerException(401, "Invalid tab ID: " + sId);
	}
		
	/**
	 * Read an XML string into a Document Object Model
	 * @param xml
	 * @return
	 * @throws ServerException
	 */
	protected Document parseXml(String xml) throws ServerException {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		    DocumentBuilder builder = factory.newDocumentBuilder();
		    InputSource is = new InputSource(new StringReader(xml));
		    return builder.parse(is);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServerException(500, e.getMessage());
		}
	}
	
	
	protected static Connection buildConnection() throws SQLServerException {
		SQLServerDataSource ds = new SQLServerDataSource();
		ds.setIntegratedSecurity(true);
		ds.setServerName("localhost");
		ds.setPortNumber(1433); 
		ds.setDatabaseName("Squirrel");
		return ds.getConnection();
	}	
}